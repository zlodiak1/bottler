from bottle import template, route


@route('/')
@route('/page1')
def page1():
    return template('templates/page1')


@route('/page2')
def page2():
    return template('templates/page2')
