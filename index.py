from bottle import run, app
from routing import *
import config 
 
run(
    app=app(),
    host=config.host,
    port=config.port,
    debug=config.debug,
    reloader=config.reloader
)